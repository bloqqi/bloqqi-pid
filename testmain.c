#include "test.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <string.h>

#define HZ (1)
#define T (1000000/HZ)
#define H (1/HZ)

static useconds_t useconds() {
  struct timeval v;
  gettimeofday(&v, NULL);
  return v.tv_sec*1000000 + v.tv_usec;
}

int main() {
  useconds_t extra = 0;
	
  Main_VARS vars = {};
  
  vars.input.SP1 = 10;
  vars.input.SP2 = 20;
  vars.input.StepTime = 5;
  vars.input.Step_1.h = H;
  
  vars.input.PV = 10;
  
  vars.input.K = 1;
  
  double simTime = 0.0;
  
  while(true) {
    useconds_t start = useconds();
    bloqqi_main(&vars);
    printf("%.0f: %.2f\n", simTime, vars.output.Out);
    fflush(stdout);
    useconds_t end = useconds();
    useconds_t run_time = end - start;

    start = end;
    useconds_t sleep_time = T - run_time - extra;
    assert(sleep_time > 0);
    usleep(sleep_time);
    end = useconds();
    useconds_t time_slept = end - start;
    if (run_time + time_slept > T) {
      extra = run_time + time_slept - T;
    } else {
      extra = 0;
    }
    simTime += H;
  }
}
