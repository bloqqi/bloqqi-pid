# Feature PID Library in Bloqqi

This repository contains the feature library for PID controllers, see file ``PIDLib.dia``.

### Run Test Code

On \*NIX:

    # Generate C code and compile C code
    $ ./gen.sh
    # Run program
    $ ./a.out
    0: 0.00
    1: 0.00
    2: 0.00
    3: 0.00
    4: 0.00
    5: 10.00
    6: 10.00
    7: 10.00
    ...

On Windows:

    # Generate C code
    > gen.bat
    # Run compiled C code
    > a.exe
